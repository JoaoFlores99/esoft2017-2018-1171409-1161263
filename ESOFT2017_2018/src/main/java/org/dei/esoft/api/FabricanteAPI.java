package org.dei.esoft.api;

import java.util.List;

public interface FabricanteAPI {

	List getModelos();

	/**
	 * 
	 * @param endLogico
	 */
	boolean validaEnderecoLogico(String endLogico);

	/**
	 * 
	 * @param modelo
	 */
	boolean validaModelo(String modelo);

}