package org.dei.esoft.model;


public interface CartaoIdentificacao {

/**
	 * 
	 * @param c
	 * @param dataFim
	 */
	AtribuicaoCartao novaAtribuicao(Colaborador c, String dataFim);

	/**
	 * 
	 * @param atr
	 */
	boolean validaAtribuicao(AtribuicaoCartao atr);

	/**
	 * 
	 * @param atr
	 */
	boolean registaAtribuicao(AtribuicaoCartao atr);

Cartao getCartao();

}