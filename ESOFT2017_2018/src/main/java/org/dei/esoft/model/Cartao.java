package org.dei.esoft.model;


public interface Cartao {

	/**
	 * 
	 * @param data
	 */
	Colaborador getColaboradorAtribuido(String data);


	/**
	 * 
	 * @param cartao_id
	 */
	boolean isCartao(int cartao_id);

	boolean isDefinitivo();

	boolean isProvisorio();

	/**
	 * 
	 * @param colab
	 * @param dataInicio
	 */
	AtribuicaoCartaoDefinitivo novaAtribuicao(int colab, int dataInicio);

	/**
	 * 
	 * @param novaAC
	 */
	void registaAtribuicao(int novaAC);

	/**
	 * 
	 * @param novaAC
	 */
	boolean validaAtribuicao(int novaAC);

}

